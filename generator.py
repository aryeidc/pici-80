#!/usr/bin/python3

# TODO: Make this file look less like an ugly hack and more like something proper.
# TODO: Combine `generate_chapters()` and `generate_data()`. They have too much in common.

SITENAME = "פיצי-80" # TODO: move to a better place (maybe some config.*?)

import jinja2
import markdown
import os
import yaml

extension_configs = {
        'codehilite': {
            'linenums': True
            },
        'toc': {
            'title': "תוכן העניינים"
            }
        }

env = jinja2.Environment(loader = jinja2.FileSystemLoader(['shared/template', 'pages', 'chapters']))

def chapter_names():
    pass # TODO: writeme!

def get_yaml(f):
    pointer = f.tell()
    if f.readline() != '---\n':
        f.seek(pointer)
        return ''
    readline = iter(f.readline, '')
    readline = iter(readline.__next__, '---\n')
    return ''.join(readline)

def extract_data(f):
    front = yaml.load(get_yaml(f))
    content = f.read()
    return front, content

def generate_chapters():
    template = env.get_template('chapter.html')
    chapteridx = 0
    #(_, _, filenames) = os.walk('chapters/').next()
    _, _, filenames = next(os.walk('chapters/'), (None, None, []))
    for filename in filenames:
        chapter = {}
        chapter['title'] = 'כותרת'#chapters[chapteridx]
        chapter['number'] = chapteridx
        infile = open(os.path.join('chapters', filename))
        print(infile)
        metadata, content = extract_data(infile)
        chapter_template = jinja2.Template(content)
        #chapter_template = env.get_template(filename)
        chapter['content'] = markdown.markdown(
                #infile.read(),
                chapter_template.render(target = 'PICO-8', chapter=chapter),
                extensions=['extra', 'codehilite', 'toc'],
                extension_configs = extension_configs
                )
        outfile = open(os.path.join('output', os.path.splitext(filename)[0]+'.html'), 'w')
        outfile.write(template.render(target='PICO-8', chapter=chapter, metadata=metadata, SITENAME=SITENAME))
        chapteridx += 1

def generate_pages():
    template = env.get_template('page.html')
    _, _, filenames = next(os.walk('pages/'), (None, None, []))
    for filename in filenames:
        page = {}
        page['title'] = 'כותרת'#chapters[chapteridx]
        infile = open(os.path.join('pages', filename))
        print(filename)
        page_template = env.get_template(filename)
        page['content'] = markdown.markdown(
                #infile.read(),
                page_template.render(),
                extensions=['extra', 'codehilite', 'toc'],
                extension_configs = extension_configs
                )
        outfile = open(os.path.join('output', os.path.splitext(filename)[0]+'.html'), 'w')
        outfile.write(template.render(target='PICO-8', page=page, metadata = None))

def generate_index():
    template = env.get_template('index.html')
    outfile = open(os.path.join('output', 'index.html'), 'w')
    outfile.write(template.render(metadata = None))

generate_chapters()
generate_index()
generate_pages()
