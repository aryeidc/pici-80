html:
	#-rm output
	-rm -r /tmp/lua > /dev/null
	mkdir /tmp/lua
	-ln -s /tmp/lua output
	cp -r shared/* output/
	cp -r carts output/
	cp -r makh-shevet output/
	cp -r chapters/*/ output/media/
	./generator.py



serve:
	cd output/; python -m http.server 8000 --bind 127.0.0.1

serve-twtonia:
	cd output/; python -m http.server 8000 --bind 192.168.0.101



view:
	xdg-open localhost:8000

view-twtonia:
	xdg-open localhost:8000


rsync:
	#TODO: write me
	#exclude __pycache__ and *.pic
